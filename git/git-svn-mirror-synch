#! /bin/sh
# Copyright (C) 2009 Sergey Poznyakoff
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

GITROOT=/gitroot
HEADPFX=

if [ -r /etc/mirror/git/config ]; then
  . /etc/mirror/git/config
fi

if [ -r $HOME/.git-mirror ]; then
  . $HOME/.git-mirror
fi  

usage() {
  cat <<-EOT
	usage: $0 GIT-REPO
EOT
}       

while [ $# -ne 0 ]
do
  case $1 in
  -h|--help)
    usage
    exit 0;;
  -*)
    echo >&2 "$0: unknown option $1"
    exit 1;;
  *)
    break;;
  esac
done

if [ $# -ne 1 ]; then
  echo >&2 "$0: required argument missing"
  exit 1
fi

BRANCHLIST=/tmp/$$.branch
TAGLIST=/tmp/$$.tag
trap 'rm -f $BRANCHLIST $TAGLIST' 1 2 3 6 13 15

cd $GITROOT/$1 || exit 1
git branch | sed 's/^\*//' | tr -d ' ' | tee $BRANCHLIST |
 while read branch
 do
   echo "Synching $branch"
   git checkout $branch
   git svn fetch
   git svn rebase -l
 done
git checkout master
git reset --hard refs/remotes/trunk

echo "Checking for new branches & tags..."
git tag > $TAGLIST
git branch -r |
 while read tag
 do
   case $tag in
   tags/*) localtag=${tag##tags/}
           if ! grep -q "^${localtag}\$" $TAGLIST; then
	     echo "Mapping $tag to $localtag"
	     git tag $localtag $tag
	   fi;;
   trunk)  ;;
   *)      if ! grep -q "^${HEADPFX}${tag}\$" $BRANCHLIST; then
             echo "Tracking $tag as ${HEADPFX}$tag"
             git branch --track ${HEADPFX}$tag $tag
	   fi;;
   esac
 done
echo "Done."
rm -f $BRANCHLIST $TAGLIST
