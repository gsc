#! /bin/sh
# Copyright 1999,2005,2007 Sergey Poznyakoff 
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program. If not, see <http://www.gnu.org/licenses/>.

# First version Id: ppp-go,v 1.2 1999/04/18 12:03:02 root Exp 

: ${PPPD:=/usr/sbin/pppd} ${CHAT:=/usr/sbin/chat}
: ${DATAPATH:=".:!PPPDIR!"} ${DIAL_SLEEP_TIME:=5s}
: ${CHAT_OPTIONS:=-v}

pname() {
	for dir in `echo $DATAPATH | tr : ' '`
	do
		if [ -r $dir/$1 ]; then
			echo $dir/$1
			break
		fi
	done
}

TMP=/tmp/chat.$$
LOCK=/var/lock/LCK.start.ppp

cleanup() {
	rm -f $TMP $LOCK
}
trap "cleanup" EXIT SIGINT SIGQUIT SIGTERM

cleanup

echo "$$" > $LOCK

dial() {
	if [ -n "$CALLBACK" ]; then
		USER=`echo "divert(-1)include(!PPPDIR!/login)divert(0)LOGIN"|m4`
		m4 -I!PPPDIR! -DNUMBER=$i -DWANT_CALLBACK=1 `pname pppscript.m4` > $TMP
		$PPPD -detach \
		      callback $CALLBACK user $USER \
		      connect "$CHAT $CHAT_OPTIONS -f $TMP"
		rc=$?
		sleep 1
	 	if [ $rc = 14 ]; then
			m4 -I!PPPDIR! -DCALLBACK `pname pppscript.m4` > $TMP
			pppd nodetach \
			     user $USER \
	                     connect "$CHAT $CHAT_OPTIONS -f $TMP"
		else
			echo "No callback"
		fi	
	else
		m4 -DNUMBER=$i `pname pppscript.m4` > $TMP
		$PPPD -detach connect "$CHAT $CHAT_OPTIONS -f $TMP"
	fi
	if [ ! -r $LOCK ]; then
		exit 1
	fi
}

while [ -r $LOCK ] 
do
	NFILE=`pname numbers`
	if [ -z "$NFILE" ]; then
		echo "Number list not specified" >&2
		break
	fi

	LINE=0	
	cat $NFILE | tr -d '-' | while read i
	do
		LINE=$(($LINE+1))
		case $i in
		\#*)	;;
		"")	;;
		[0-9]*)	dial $i
			sleep $DIAL_SLEEP_TIME
			;;
		*)	echo "$NFILE:$LINE: Bad line" >&2;;
		esac		       
	done
done

cleanup

# End of start-ppp
