dnl Copyright 1999 Sergey Poznyakoff 
dnl
dnl This program is free software; you can redistribute it and/or modify
dnl it under the terms of the GNU General Public License as published by
dnl the Free Software Foundation; either version 3, or (at your option)
dnl any later version.
dnl
dnl This program is distributed in the hope that it will be useful,
dnl but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
dnl GNU General Public License for more details.
dnl
dnl You should have received a copy of the GNU General Public License
dnl along with this program. If not, see <http://www.gnu.org/licenses/>.
dnl ########################################################################
dnl
dnl This is the default script file for start-ppp. In the majority of
dnl cases you won't need to modify it, edit files `modem' and `login'
dnl instead.
dnl
divert(-1)
include(/etc/ppp/modem)
include(/etc/ppp/login)
divert(0)dnl
TIMEOUT ifdef(`MODEM_TIMEOUT', MODEM_TIMEOUT, 60)
ABORT ERROR
ABORT BUSY
ifdef(`CALLBACK',`dnl
RING	""
CONNECT ""
',`dnl
ABORT "NO CARRIER"
ABORT "NO DIAL TONE"
"" "ifdef(`MODEM_INITSTRING', MODEM_INITSTRING, AT&F1)`'ifdef(`WANT_CALLBACK',`S0='WANT_CALLBACK)" dnl
OK "atd`'ifdef(`MODEM_DIALPREFIX',MODEM_DIALPREFIX,t)`'ifdef(`AREA_CODE',AREA_CODE)`'NUMBER" dnl
CONNECT "" dnl
ogin: LOGIN dnl
assword: PASSWORD 
')
