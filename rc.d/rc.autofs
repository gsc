#! /bin/sh
# This script is based on rc.autofs.in from autofs-3.1.7 package

# Configurable variables:
DAEMON=/usr/sbin/automount

CFGDIR=/etc/automount
CFGFILE=$CFGDIR/master
LOCKFILE=/var/lock/subsys/autofs

# Adjust it if necessary
PS="ps axww"

################# !!! NOTHING TO MODIFY BELOW THIS LINE !!! ###################


#
# This function will build a list of automount commands to execute in
# order to activate all the mount points. It is used to figure out
# the difference of automount points in case of a reload
#
getmounts() {
   	# Check for local maps to be loaded
	if [ -f $CFGFILE ]; then
    		cat $CFGFILE | sed '/^#/d;/^$/d' | 
		while read dir map options
		do
			if [ ! -z "$dir" -a ! -z "$map" \
				-a x`echo "$map" | cut -c1` != 'x-' ]; then
				map=`echo "$CFGDIR/$map" | sed 's:^$CFGDIR//:/:'`
				# Split options into daemon proper and
				# map options (separated by a double-slash)
				mopt=
				dopt=
				opt=dopt
				for i in $options
				do
					if [ "$i" = "--" ]; then
						opt=mopt
					else
						eval $opt="\"\${$opt} $i\""
						#eval $x
					fi
				done

				if [ -x $map ]; then
					echo "$DAEMON $dopt $dir program $map $mopt"
				elif [ -f $map ]; then
					echo "$DAEMON $dopt $dir file $map $mopt"
				else
					# FIXME: this is a bit illogical, since
					# map and its options are separated in
					# the config. But I don't use it,
					# anyway.
					echo "$DAEMON $dopt $dir `basename $map` $mopt"
				fi
			fi
		done
	else
		echo "$0: $CFGFILE not found" >&2
		exit 1
	fi
}

printheader() {
	echo $*
	echo $* | sed 's/./-/g'
}

#
# Status lister.
#
status() {
	printheader "Configured Mount Points:"
	getmounts | tr -s ' \t' ' '
	echo ""
	printheader "Active Mount Points:"
	$PS | grep "[0-9]:[0-9][0-9] $DAEMON " | 
		while read pid tt stat time command
		do
			echo $pid $command
		done
}


# MAIN

case "$1" in
  start)
	# Check if the automounter is already running?
	if [ ! -f $LOCKFILE ]; then
	    echo 'Starting automounter: '
	    getmounts | sh
	    touch $LOCKFILE
	fi
	;;
  stop)
	kill -TERM $(/sbin/pidof $DAEMON)
	rm -f $LOCKFILE
	;;
  restart)
        if [ ! -f $LOCKFILE ]; then
                echo "Automounter not running"
        else
		$0 stop
	fi
	$0 start;;
  reload)
	if [ ! -f $LOCKFILE ]; then
		echo "Automounter not running"
		exit 1
	fi
	echo "Checking for changes to $CFGFILE ...."
        TMP1="/tmp/autofs.1.$$"
        TMP2="/tmp/autofs.2.$$"
	getmounts | tr -s ' \t' ' ' >$TMP1
	cat /dev/null > $TMP2
	$PS | grep "[0-9]:[0-9][0-9] $DAEMON " | 
	    while read pid tt stat time command
	    do
		echo "$command" >>$TMP2
		if ! grep -q "^$command" $TMP1; then
			echo "Stop $command"
			kill -USR2 $pid
		fi
	    done
        
	cat $TMP1 |
	    while read x
	    do
		if ! grep -q "^$x" $TMP2; then
			echo "Start $x"
			eval $x
		fi
	    done 
	rm -f $TMP1 $TMP2
	;;
  status)
	status
	;;
  get)  getmounts;;  # For debugging only. Do not announce below.
  *)
	echo "Usage: $0 {start|stop|restart|reload|status}"
	exit 1
esac
