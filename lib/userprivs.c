/* This file is part of GSC
   Copyright (C) 2007 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include "xalloc.h"

static const char *errfmt;
static unsigned long errarg[3];
static int error_code;

static void
save_error (int ec, const char *fmt,
	    unsigned long x0, unsigned long x1, unsigned long x2)
{
  error_code = ec;
  errfmt = fmt;
  errarg[0] = x0;
  errarg[0] = x1;
  errarg[0] = x2;
}

char *
gsc_userprivs_errstring ()
{
  static char *buf = NULL;
  static size_t size = 10;
  int done = 0;
  
  if (!buf)
    buf = x2realloc (NULL, &size);
  
  do
    {
      size_t n = snprintf (buf, size, errfmt, errarg[0], errarg[1], errarg[2]);
      if (n < 0 || n >= size || !memchr (buf, '\0', n + 1))
	{
	  size *= 2;
	  buf = x2realloc (buf, &size);
	}
      else
	done = 1;
    }
  while (!done);

  if (error_code)
    {
      const char *s = strerror (error_code);
      size_t len = strlen (buf) + 2 + strlen (s) + 1;
      if (len > size)
	{
	  size = len;
	  buf = x2realloc (buf, &size);
	}
      strcat (buf, ": ");
      strcat (buf, s);
    }
  return buf;
}

int
gsc_userprivs (uid_t uid, gid_t *grplist, size_t ngrp)
{
  gid_t gid = grplist[0];
  int rc = 0;
  size_t size = 1, j = 1;

  if (uid == 0)
    return 0;
  
  /* Reset group permissions */
  if (geteuid () == 0 && setgroups (ngrp, grplist))
    {
      save_error (errno, "setgroups(%lu, %lu...) failed",
		  ngrp, grplist[0], 0);
      rc = 1;
    }
	
  /* Switch to the user's gid. On some OSes the effective gid must
     be reset first */

#if defined(HAVE_SETEGID)
  if ((rc = setegid (gid)) < 0)
    save_error (errno, "setegid(%lu) failed", gid, 0, 0);
#elif defined(HAVE_SETREGID)
  if ((rc = setregid (gid, gid)) < 0)
    save_error (errno, "setregid(%lu,%lu) failed",
		(unsigned long) gid, (unsigned long) gid, 0);
#elif defined(HAVE_SETRESGID)
  if ((rc = setresgid (gid, gid, gid)) < 0)
    save_error (errno, "setresgid(%lu,%lu,%lu) failed",
		(unsigned long) gid, (unsigned long) gid, (unsigned long) gid);
#endif

  if (rc == 0 && gid != 0)
    {
      if ((rc = setgid (gid)) < 0 && getegid () != gid) 
	save_error (errno, "setgid(%lu) failed", (unsigned long) gid, 0, 0);
      if (rc == 0 && getegid () != gid)
	{
	  save_error (0, "Cannot set effective gid to %lu",
		      (unsigned long) gid, 0, 0);
	  rc = 1;
	}
    }

  /* Now reset uid */
  if (rc == 0 && uid != 0)
    {
      uid_t euid;

      if (setuid (uid)
	  || geteuid () != uid
	  || (getuid () != uid
	      && (geteuid () == 0 || getuid () == 0)))
	{
			
#if defined(HAVE_SETREUID)
	  if (geteuid () != uid)
	    {
	      if (setreuid (uid, -1) < 0)
		{ 
		  save_error (errno, "setreuid(%lu,-1) failed",
			      (unsigned long) uid, 0, 0);
		  rc = 1;
		}
	      if (setuid (uid) < 0)
		{
		  save_error (errno, "second setuid(%lu) failed",
			      (unsigned long) uid, 0, 0);
		  rc = 1;
		}
	    }
	  else
#endif
	    {
	      save_error (errno, "setuid(%lu) failed",
			  (unsigned long) uid, 0, 0);
	      rc = 1;
	    }
	}
	
      euid = geteuid ();
      if (uid != 0 && setuid (0) == 0)
	{
	  save_error (0, "seteuid(0) succeeded when it should not", 0, 0, 0);
	  rc = 1;
	}
      else if (uid != euid && setuid (euid) == 0)
	{
	  save_error (0, "Cannot drop non-root setuid privileges", 0, 0, 0);
	  rc = 1;
	}

    }
  
  return rc;
}
