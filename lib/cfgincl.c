/* This file is part of GSC
   Copyright (C) 2007 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include "xalloc.h"
#include "gsc.h"

char *gsc_config_include_dir;

static gsc_config_file_t *
find_stat (struct stat *st, gsc_config_file_t *file)
{
  for (; file; file = file->prev)
    if (st->st_dev == file->dev && st->st_ino == file->ino)
      break;
  return file;
}

static void
get_include_dir (gsc_config_file_t *file, const char **scdir, size_t *sclen)
{
  char *p;

  if (gsc_config_include_dir)
    {
      *scdir = gsc_config_include_dir;
      *sclen = strlen (gsc_config_include_dir);
      return;
    }
  
  for (; file->prev; file = file->prev)
    ;
  p = strrchr (file->file_name, '/');
  if (!p)
    {
      *scdir = ".";
      *sclen = 1;
    }
  else
    {
      *scdir = file->file_name;
      while (p >= file->file_name && *p == '/')
	p--;
      *sclen = p + 1 - file->file_name;
    }
}

int
gsc_config_include_start (gsc_config_file_t *new_file,
			  gsc_config_file_t *file, char *name)
{
  gsc_config_file_t *p;
  struct stat st;
  char *namebuf;

  if (name[0] != '/')
    {
      const char *scdir;
      size_t sclen;
      size_t len;
      
      get_include_dir (file, &scdir, &sclen);
      len = sclen + 1 + strlen (name) + 1;
      namebuf = malloc (len);
      if (!namebuf)
	{
	  file->error_msg (file->file_name, file->line,
			   "cannot create a full file name for `%s': %s",
			   name, strerror (ENOMEM));
	  file->error_count++;
	  return 1;
	}
      memcpy (namebuf, scdir, sclen);
      namebuf[sclen] = '/';
      strcpy (namebuf + sclen + 1, name);
    }
  else
    {
      namebuf = strdup (name);
      if (!namebuf)
	{
	  file->error_msg (file->file_name, file->line,
			   "cannot store include file name: %s",
			   strerror (ENOMEM));
	  file->error_count++;
	  return 1;
	}
    }

  if (stat (namebuf, &st))
    {
      file->error_msg (file->file_name, file->line,
		       "cannot stat `%s': %s", namebuf, strerror (errno));
      file->error_count++;
      free (namebuf);
      return 1;
    }

  if ((p = find_stat (&st, file)))
    {
      file->error_msg (file->file_name, file->line, "recursive inclusion");
      if (p->prev)
	file->error_msg (p->prev->file_name, p->prev->line,
			 "`%s' already included here",
			 namebuf);
      file->error_count++;
      free (namebuf);
      return 1;
    }
  
  new_file->fp = fopen (namebuf, "r");
  if (!new_file->fp)
    {
      file->error_msg (file->file_name, file->line, "cannot open `%s': %s",
		       namebuf, strerror (errno));
      file->error_count++;
      free (namebuf);
      return 1;  
    }

  new_file->buf = NULL;
  new_file->size = 0;
  new_file->line = 0;
  new_file->ino = st.st_ino;
  new_file->dev = st.st_dev;
  new_file->prev = file;
  new_file->file_name = namebuf;
  new_file->error_msg = file->error_msg;
  new_file->error_count = file->error_count;

  return 0;
}

gsc_config_file_t *
gsc_config_include_stop (gsc_config_file_t *file)
{
  free (file->buf);
  free ((char*) file->file_name);
  fclose (file->fp);
  
  file->prev->error_count = file->error_count;
  return file->prev;
}

void
gsc_config_include_handler (gsc_config_file_t *file, char *kw, char *val,
			    void *data)
{
  gsc_config_file_t new_file;

  if (gsc_config_include_start (&new_file, file, val))
    return;
  
  gsc_config_parse_block (&new_file, data, file->kwtab, NULL);

  gsc_config_include_stop (&new_file);
}

