2011-10-29  Sergey Poznyakoff  <gray@gnu.org.ua>

	* upload/gnupload: Allow user@download.gnu.org.ua as a target.
	Use canonical hostname (download.gnu.org.ua).
	* cvs/sv_logger: Remove.
	* mc: Remove.
	* fsf-move/fsf-move: Use `tar cf' to check for its presence.
	* cvs/sv_sync_www_schedule: Use logger
	* cvs/sv_www_loginfo: Likewise.
	* doc/gsc.texi: Update.
	
2010-01-29  Sergey Poznyakoff  <gray@gnu.org.ua>

	* jabberd: Removed. Use GNU pies instead.
	* Makefile.am, configure.ac: Update.
	* doc/gsc.texi: Update.

2009-08-03  Sergey Poznyakoff  <gray@gnu.org.ua>

	* vpn/svpnsh: Fix concurrent login recognition.

2009-03-19  Sergey Poznyakoff  <gray@gnu.org.ua>

	Bugfix in sv_sync_www
	
	* cvs/sv_sync_www.c (do_sync): Do not sort the array.  This causes
	malfunction on newly committed hierarchies.

2009-02-13  Sergey Poznyakoff  <gray@gnu.org.ua>

	Bugfix

	* git-svn-mirror-synch: Call git reset.
	* git/git-svn-mirror-finish: Install SVN hook and add a README.git
	file to SVN.

2009-01-24  Sergey Poznyakoff  <gray@gnu.org.ua>

	Improve gnupload
	
	* upload/gnupload: Implement --delete, --rmsymlink and --symlink.
	Rename old `--symlink' option to `--symlink-re'.
	Read configuration file at startup.

2009-01-05  Sergey Poznyakoff  <gray@gnu.org.ua>

	* git: New directory
	* git/git-svn-mirror-create: New file.
	* git/git-svn-mirror-finish: New file.
	* git/git-svn-mirror-synch: New file.

2008-10-15  Sergey Poznyakoff  <gray@gnu.org.ua>

	* upload/gnupload: Accept --symlink and --dry-run options.

2008-10-09  Sergey Poznyakoff  <gray@gnu.org.ua>

	* upload/gnupload, rc.d/rc.local: Use ${X#... where necessary.
	* rc.d/rc.local: Accept optional component names on the command
	line.
	
2008-10-07  Sergey Poznyakoff  <gray@gnu.org.ua>

	* rc.d/start-stop: New option -P
	(status): Check for non-empty PID
	(start): Do not start if already running. Use status to verify.
	* rc.d/rc.local (read_section): Remove leading and trailing white
	space. 
	* rc.d/rc.tagr: Pass -P option to start-stop

	* rc.d/rc.local (reverse): Use sed to reverse the input.

2008-10-06  Sergey Poznyakoff  <gray@gnu.org.ua>

	* rc.d/rc.local: Rewrite using configuration file.  Implement new
	options (-d, --directory, --config, --restart).
	* rc.d/local.conf: New file.

2008-09-26  Sergey Poznyakoff  <gray@gnu.org.ua>

	* vpn/svpn: New option -d.
	* etc/links.txt: Update.

2008-08-23  Sergey Poznyakoff  <gray@gnu.org.ua>

	* ssl: New directory.
	* ssl/ssltool: New file.

2008-07-12  Sergey Poznyakoff  <gray@gnu.org.ua>

	* svn/svn-import-cvs: Fix tar command line. 
	* maint/processlogs: Do not call logrotate in dry-run mode.
	
2008-05-02  Sergey Poznyakoff  <gray@gnu.org.ua>

	* maint/processlogs (TMPDIR): Allow to set from the command line.
	(sortlog): Fix regular expressions for both log formats.
	(SKIP_LOGROTATE): New variable.

2008-04-23  Sergey Poznyakoff  <gray@gnu.org.ua>

	* cvs/mksnapshot: Minor changes.

2008-04-09  Sergey Poznyakoff  <gray@gnu.org.ua>

	* cvs/mksnapshot: Support git.

2008-03-14  Sergey Poznyakoff  <gray@gnu.org.ua>

	* etc/links.txt: Update

2008-03-12  Sergey Poznyakoff  <gray@gnu.org.ua>

	* rc.d/start-stop (start): Fix typo.

	* upload/gnupload: New file. 

	* maint/processlogs: Add to the repository.

2007-10-07  Sergey Poznyakoff  <gray@gnu.org.ua>

	* etc/links.txt: Update

2007-09-25  Sergey Poznyakoff  <gray@gnu.org.ua>

	* cvs/mksnapshot (MSG): Fix initialization

2007-09-24  Sergey Poznyakoff  <gray@gnu.org.ua>

	* cvs/mksnapshot (COMPRESS,ZSUFFIX,VERBOSE): New configuration
	variables.
	(module): Abort creating snapshot if get_sources returned
	failure. Report it, if VERBOSE is set.
	Compression method is configurable via the COMPRESS variable.
	* doc/gsc.texi: Update the desctiption of mksnapshot.
	
2007-09-05  Sergey Poznyakoff  <gray@gnu.org.ua>

	* lib/gsc.h: Keep the namespace clean.
	(gsc_config_include_dir): New variable
	* lib/cfgincl.c (gsc_config_include_dir): New variable
	(cfg_include_start): Rename to gsc_config_include_start;
	use gsc_config_include_dir if the file name given is not
	absolute.
	(cfg_include_stop): Rename to gsc_config_include_stop
	(cfg_include_handlera): Rename to gsc_config_include_handler

2007-09-04  Sergey Poznyakoff  <gray@gnu.org.ua>

	* configure.ac, Makefile.am: Remove wydawca
	* lib/cfg.c (find_cfg_fun): Implement default handler
	(read_cont_line): Fix reading lines that not terminate with a
	newline.
	(gsc_config_parse): Initialize new gsc_config_file_t fields
	* lib/gsc.h (gsc_config_file_t): New fields dev, ino, prev and
	kwtab.
	(cfg_include_handler, cfg_include_start, cfg_include_stop): new
	functions.
	* lib/cfgincl.c: New file. Implementation of include feature in
	configuration files.
	* lib/Makefile.am: Add cfgincl.c

2007-09-01  Sergey Poznyakoff  <gray@gnu.org.ua>

	Wydawca moved to a separate project
	(http://puszcza.gnu.org.ua/projects/wydawca)

2007-08-30  Sergey Poznyakoff  <gray@gnu.org.ua>

	* configure.ac: Check for perl
	* cvs/sv_post_office.pl: New file
	* cvs/sv_mailman_gray.pl: New file
	* cvs/Makefile.am: Add new files

2007-08-26  Sergey Poznyakoff  <gray@gnu.org.ua>

	* wydawca/wydawca.h (struct file_triplet): New member user
	(fill_project_name): New function
	* wydawca/verify.c (get_project_name): Rename to
	fill_project_name, remove static qualifier.
	* wydawca/wydawca.rc: Update
	* wydawca/mail.c (notify_owner): Notify only the submitter on
	ev_success, ev_bad_directive_signature and
	ev_bad_detached_signature (should it be made configurable?)
	(notify): Call fill_project_name
	* wydawca/triplet.c (hash_triplet_free): Free trp->user
	(expand_user_real_name, expand_user_email): Change data indices to
	match those of project_owner_method.
	
	Implement project owner notifications.
	
	* wydawca/wydawca.c (syslog_printer): Reduce the number of memory
	reallocations
	(make_stat_expansion): Update
	* wydawca/method.c: Implement a new framework: methods may return
	2-dimensional arrays of strings.
	* wydawca/sql.c, wydawca/sql.h: Implement the new method framework.
	* wydawca/verify.c (expand_param): kw_expansion may provide
	expansion functions. An additional argument supplies user-defined
	data for expansions.
	(escape_kwexp): Extern
	(free_kwexp): Improve
	(get_project_name): New function
	(make_default_kwexp): New function
	(check_access_rights): Call get_project_name. Use
	make_default_kwexp to initialize expansions
	(verify_directive_file): Use make_default_kwexp to initialize
	expansions
	* wydawca/wydawca.h (NITEMS): New macro
	(enum access_method_type): New members: ncol, nrow
	(struct directory_pair): New members url,project_owner_method,
	user_data_method
	(struct file_info): Replace mtime, uid with struct stat sb
	(struct file_triplet): New members project, dpair, user_data
	(TRIPLET_UID): Take into account the changes to struct file_info
	(enum notification_event): New data type
	(notify_project_owner, notify_admin, notify): New functions
	(struct kw_expansion): New members static_p, expand and data.
	(escape_kwexp,make_default_kwexp): New proto
	(expand_param): Change signature
	(triplet_expand_param): New function
	(method_result): Change prototype
	(method_num_rows,method_num_cols): New functions
	* wydawca/config.c: New statements project-owner, user-data,
	admin-address, mail-user, user-message
	directory can take an optional argument specifying base URL for
	notification messages
	* wydawca/gpg.c (verify_directive_signature): Expand directives
	even if the signature does not match. Useful for notifications.
	Add notifications.
	* wydawca/process.c: Add notifications.
	* wydawca/directive.c: Add notifications
	* wydawca/wydawca.rc: Update
	* wydawca/mail.c, wydawca/mail.h: Implement project owner
	notifications 
	* wydawca/triplet.c (triplet_expand_param): New function

	* lib/cfg.c (read_cont_line): Fix counting of input lines.
	
2007-08-25  Sergey Poznyakoff  <gray@gnu.org.ua>

	* configure.ac: Require mailutils for wydawca
	* bootstrap: Require inttostr and strftime 
	* wydawca/mail.h: New file
	* wydawca/mail.c: New file
	* wydawca/Makefile.am: Add mail.c and mail.h
	* wydawca/wydawca.c: Include mail.h
	(stat_mask_p, make_stat_expansion): New functions
	(logstats): Call mail_stats
	(main): Call initialize mailer subsystem

	* wydawca/sql.c, wydawca/sql.h: Keep usage reference count. Do not
	deinitialize unless it falls to 0. Do not initialize if it is > 0.

	* wydawca/verify.c (expand_param): Rewrite to allow long keywords
	All callers updated.
	* wydawca/wydawca.h (struct access_method): Keep reference count
	(struct directory_pair): verify_method and gpg_key_method are
	pointers to structs.
	(struct kw_expansion): kw is char*
	(count_collected_triplets): New function
	(method_new): New function
	* wydawca/config.c: reimplement  verify-user and gpg-key
	New keywords mailer, admin-address, from-address, mail-admin-stat
	and admin-stat-message
	* wydawca/process.c: Close methods only when their reference count
	is 0.
	* wydawca/method.c: Likewise.
	(method_new): New function
	* wydawca/wydawca.rc: Update
	* wydawca/diskio.c: Minor changes
	* wydawca/triplet.c (count_collected_triplets): New function
	* jabberd/main.c: Minor change
	
	* wydawca/wydawca.c, wydawca/config.c, wydawca/gpg.c: New stat
	types STAT_ACCESS_VIOLATIONS and STAT_BAD_SIGNATURES.
	* wydawca/wydawca.h (expand_param): New function
	(struct kw_expansion): New data type
	* wydawca/verify.c (expand_param): Rewrite.
	(verify_directive_file): Call check_access_rights.

2007-08-24  Sergey Poznyakoff  <gray@gnu.org.ua>

	* lib/version.c: New file
	* lib/gsc.h (gsc_version): New function
	* cvs/Makefile.am, ckaliases/Makefile.am, wydawca/Makefile.am,
	lib/Makefile.am, jabberd/Makefile.am: Fix include dirs
	* ckaliases/ckaliases.c, ckaliases/lex.l, ckaliases/ckaliases.h,
	ckaliases/gram.y: Use long options. Implement --version
	* wydawca/wydawca.c, jabberd/main.c: Use gsc_version to display
	program version.
	* doc/gsc.texi: Update
	* bootstrap: Add vasprintf

	* README-alpha, README-hacking: New files.
	* bootstrap: Improve

	Implement final statistics.
	
	* wydawca/wydawca.c (log_to_stderr): Init to -1 (autodetect)
	(wydawca_stat,syslog_include_prio,print_stats): New variables
	(version): Update
	(main): New options --cron, --syslog
	Autodetect log_to_stderr value, unless set explicitly
	(syslog_printer): Output priority string if requested by
	syslog_include_prio
	Print final statistics
	(logmsg): Update statistics
	(logstats): New function
	* wydawca/wydawca.h (enum wydawca_stat): New type
	(STAT_MASK, STAT_MASK_NONE, STAT_MASK_ALL, UPDATE_STATS): New defines
	(syslog_include_prio,wydawca_stat,print_stats): New globals
	* wydawca/config.c: New statements "syslog-print-priority" and
	"statistics"
	(get_backup_version): Fix handling of ARGMATCH result.
	* wydawca/directive.c: Update statistics
	* wydawca/diskio.c: Likewise
	* wydawca/triplet.c: Likewise
	* wydawca/wydawca.rc: Update
	
2007-08-23  Sergey Poznyakoff  <gray@gnu.org.ua>

	Improve safety checks; implement symlink/rmsymlink/archive
	directives; Fix directive signature verification.

2007-08-22  Sergey Poznyakoff  <gray@gnu.org.ua>

	* wydawca/exec.c: New file
	* wydawca/diskio.c, wydawca/directive.c: Implement all directives

2007-08-21  Sergey Poznyakoff  <gray@gnu.org.ua>

	* wydawca/wydawca.c (usage): Update
	New option --lint
	(tar_command_name): New variable
	* wydawca/wydawca.h: include backupfile.h
	(enum archive_type,struct archive_descr): New data types
	(struct directory_pair.archive): New member
	(safe_file_name): New function
	(move_file): Redo prototype
	(start_prog,log_output): New declarations
	* wydawca/config.c: Include argmatch.h
	(safe_file_name): New function
	(archive,umask,tar-program): New configuration file statemenst
	* wydawca/gpg.c (start_prog,log_output): Remove static qualifiers
	* wydawca/directive.c (process_directives): Update
	* wydawca/wydawca.rc: Update
	* wydawca/diskio.c (move_file): Implement archiving and backups
	* bootstrap: Require backupfile
	
2007-08-20  Sergey Poznyakoff  <gray@gnu.org.ua>

	* jabberd/Makefile.am, cvs/Makefile.am, ckaliases/Makefile.am,
	acinclude.m4, Makefile.am: Add copyright statements
	* wydawca/sql.c (trim_length): Move to verify.c
	* wydawca/verify.c (trim_length, trim)
	(check_access_rights): New functions
	(expand_param): Export
	(verify_triplet): Call verify_directive_format and
	check_access_rights
	* wydawca/wydawca.h (trim_length, trim, directive_parse)
	(directive_get_value, directive_pack_version)
	(directive_version_in_range_p, verify_directive_format)
	(directive_first, directive_next, process_directives)
	(create_directory, move_file): New functions
	(MIN_DIRECTIVE_VERSION,MAX_DIRECTIVE_VERSION)
	(MKDIR_PERMISSIONS,CREAT_PERMISSIONS): New defines
	* wydawca/gpg.c (wydawca_gpg_homedir): Return a meaningful value
	(verify_directive_signature): Call directive_parse
	* wydawca/process.c (parse_file_name): Return void
	* wydawca/directive.c: New file
	* wydawca/diskio.c: New file
	* wydawca/triplet.c (triplet_processor): Call process_directives
	(enumerate_triplets): Arg is not const
	* wydawca/Makefile.am: Add new sources
	* configure.ac: Check for sendfile

	* jabberd/jabberd.h (getmaxfd): moved to gsc.h
	* jabberd/main.c (stderr_printer): Minor fix
	* wydawca/triplet.c (triplet_processor): Minor fix
	* wydawca/verify.c (verify_triplet): Save owner gid in reg.
	Call verify_detached_signature
	* wydawca/wydawca.h (struct file_register): New member gid.
	(verify_detached_signature): New function
	* wydawca/gpg.c (verify_detached_signature): New function
	* lib/Makefile.am: Add userprivs.c
	* lib/userprivs.c: New file
	
2007-08-19  Sergey Poznyakoff  <gray@gnu.org.ua>

	* wydawca: New module
	* lib: New directory

2007-08-07  Sergey Poznyakoff  <gray@gnu.org.ua>

	Change license to GPLv3
	
2007-08-07  Sergey Poznyakoff  <gray@gnu.org.ua>

	* fsf-move/fsf-move.awk: Get s/r pairs from the file
	* fsf-move/fsf-move: -3 affects both version number and postal
	address 

2007-08-06  Sergey Poznyakoff  <gray@gnu.org.ua>

	* fsf-move/fsf-move: New options -R and -S
	* fsf-move/fsf-move.awk (BEGIN): SEARCHRE and REPLACE are given
	from the command line.
	
	* fsf-move/fsf-move: New option -3 to relicense from GPLv2 to
	GPLv3
	* fsf-move/fsf-move.awk: Support relicensing GPLv2->GPLv3
	Update copyright year if necessary.

2007-07-03  Sergey Poznyakoff  <gray@gnu.org.ua>

	* doc/gsc.texi, etc/links.txt: Update

2007-06-06  Sergey Poznyakoff  <gray@gnu.org.ua>

	* doc/gsc.texi (mksnapshot): Update
	* cvs/mksnapshot: Handle Savane-style SVN repositories

2007-06-05  Sergey Poznyakoff  <gray@gnu.org.ua>

	* jabberd/jabberd.h: Fix typo
	* jabberd/progman.c (struct prog.pidfile): New member
	(register_prog): Take 6th argument, specifying the file to be
	deleted before startup.
	(register_transport, register_jabber_process): Update calls to
	register_prog 
	(prog_start): Remove pidfile if requested
	(progman_dump_stats): Minor fix
	* jabberd/main.c: New configuration keyword (in `transport'
	section): `pidfile'
	* doc/gsc.texi: Update
	
2007-06-04  Sergey Poznyakoff  <gray@gnu.org.ua>

	* jabberd/main.c: Implement long options, running instance
	control and component dependency checking
	* jabberd/Makefile.am (AM_CPPFLAGS): Pass statedir.
	* jabberd/jabberd.h (register_prog): Remove
	(register_trasport): New proto
	(progman_stop_component, progman_dump_stats): New proto
	* jabberd/progman.c (struct prog.depend): New member
	(TYPE_PROG): Rename to TYPE_CORE
	(register_prog): Change signature. Store dependency information.
	(register_transport): New function
	(prog_stop): New function
	(progman_stop_component,progman_dump_stats): New functions
	* doc/gsc.texi: Update
	
2007-06-03  Sergey Poznyakoff  <gray@gnu.org.ua>

	* doc/gsc.texi: Document jabberd.
	* jabberd/main.c: New statement `umask'.

2007-06-02  Sergey Poznyakoff  <gray@gnu.org.ua>

	* jabberd/jabberd.h, jabberd/progman.c, jabberd/main.c: New
	configuration block statement `exec'.
	* jabberd/argcv.c: New file
	* jabberd/Makefile.am: Update

	* svn: New directory
	* svn/svn-import-cvs: New file

2007-06-01  Sergey Poznyakoff  <gray@gnu.org.ua>

	* vpn: New directory
	* vnp/svpn, vpn/svpnsh: New files.

2007-05-31  Sergey Poznyakoff  <gray@gnu.org.ua>

	* Makefile.am, configure.ac: Add jabberd
	* jabberd/Makefile.am: New file
	* jabberd/jabberd.h: New file
	* jabberd/main.c: New file
	* jabberd/progman.c: New file

2007-03-17  Sergey Poznyakoff  <gray@gnu.org.ua>

	* etc/links.txt: Updated

2007-01-29  Sergey Poznyakoff  <gray@gnu.org.ua>

	* ppp/start-ppp.sh, ppp/pppscript.m4: Use callback

2007-01-10  Sergey Poznyakoff  <gray@gnu.org.ua>

	* maint/svn-backup: rename -m to -M
	Add new option -m with the same meaning as in svn

2006-12-26  Sergey Poznyakoff  <gray@gnu.org.ua>

	* maint/svn-backup: New file

2006-12-12  Sergey Poznyakoff  <gray@gnu.org.ua>

	* rc.d/rc.ntpd: Use spaces in arithmetical operations
	* rc.d/rc.tagr: Rewrite using start-stop
	* rc.d/start-stop: Generic rc script

2006-09-30  Sergey Poznyakoff  <gray@gnu.org.ua>

	* firewall/firewall.m4 (port_setup): Allow shell variables in 
	rule files.
	Reindented.
	* rc.d/rc.firewall: New option -x

2006-09-21  Sergey Poznyakoff  <gray@gnu.org.ua>

	* mc/mirddin.mc: Add X-Mailutils-Message-Id, reconfigure
	LOCAL_MAILER_ARGS
	* mc/relay1.mc: Add missing LOCAL_CONFIG
	* mc/relay2.mc: Likewise
	* rc.d/rc.local (run_local): Use eval
	* rc.d/rc.inet1: Accept start & stop arguments

2006-09-04  Sergey Poznyakoff  <gray@gnu.org.ua> 

	* ckaliases/lex.l: Accept commas
	* cvs/sv_sync_www.c: New option --verbose (-v) controls the
	verbosity level.
	* mc/ulysses.mc: Local rule for mailfromd testing

2006-08-14  Sergey Poznyakoff  <gray@gnu.org.ua>

	* etc/links.txt: New chapter: Library

2006-07-28  Sergey Poznyakoff  <gray@gnu.org.ua>

	* elisp/links-mode.el: Change node structure: add anchor field.
	All references updated.
	(get-token): Optional [word] after leading asterisks sets
	anchor name for referencing the entry from outside.
	(scan-reference,scan-text): Support anchors.
	* etc/links.txt: Add constant anchors
	* mc/relay1.mc: Update

2006-06-20  Sergey Poznyakoff  <gray@gnu.org.ua>

	* elisp/emacs.el: Cleanup

2006-06-08  Sergey Poznyakoff  <gray@gnu.org.ua>

	* etc/links.txt: Update

2006-06-03  Sergey Poznyakoff  <gray@gnu.org.ua>

	* etc/links.txt: Update

2006-04-17  Sergey Poznyakoff  <gray@gnu.org.ua>

	* etc/links.txt: Update

2006-04-03  Sergey Poznyakoff  <gray@gnu.org.ua>

	* etc/links.txt: Update

2006-02-12  Sergey Poznyakoff  <gray@gnu.org.ua>

	* configure.ac: AC_TYPE_SIGNAL
	* cvs/sv_sync_www.c (main): Remove spurious check for savane_gid.
	(sync_www): Make sure the symlinks phase does not return spurious
	error code.
	* cvs/sv_sync_www_schedule (JOBFILE): Use plain username

2006-02-09  Sergey Poznyakoff  <gray@gnu.org.ua>

	* mc/ulysses.mc: Place H statements after LOCAL_CONFIG

	* maint/Makefile.am: Add heading comment

	* cvs/sv_sync_www_schedule: Use a single file per job.

	* doc/gsc.texi: Removed section about sv_sync_www_flush 

	* cvs/sv_sync_www.c: Rewritten to incorporate the functionality of
	sv_sync_www_flush and to make updates more safe.

	* cvs/sv_sync_www_flush: Removed
	
	* cvs/Makefile.am (bin_SCRIPTS): Remove sv_sync_www_flush

2006-01-08  Sergey Poznyakoff  <gray@gnu.org.ua>

	* etc/links.txt: Update

2005-12-27  Sergey Poznyakoff  <gray@gnu.org.ua>

	* elisp/dot.emacs: Set up po-subedit-exit-hook
	* elisp/po.el (gray-po-subedit-mode-hook): Set
	ispell-local-dictionary.
	(gray-po-subedit-exit-hook): New hook.

2005-12-25  Sergey Poznyakoff  <gray@gnu.org.ua>

	* elisp/po.el (po-ispell-input-methods): Update
	(process-flag, process-format): New functions
	(po-ispell-run): Process pragmatic comments (#,).
	Ignore printf-style format specifications in msgstr's.
	Correctly handle ngettext entries.

2005-11-29  Sergey Poznyakoff  <gray@gnu.org.ua>

	* etc/links.txt: Update

2005-10-12  Sergey Poznyakoff  <gray@Ulysses.farlep.net>

	* maint: New directory
	* maint/session-cleanup: New file
	* maint/Makefile.am: New file
	* maint/.cvsignore: New file
	* configure.ac: Add maint/
	* Makefile.am: Likewise
	* doc/gsc.texi (session-cleanup): New section

2005-10-01  Sergey Poznyakoff  <gray@gnu.org.ua>

	* firewall/firewall.m4 (__rule__): Always add --protocol all if
	either source or destination port is specified.

2005-09-30  Sergey Poznyakoff  <gray@gnu.org.ua>

	* bind/master/bin/ndc.awk: Change pragmatic comments markers 
	to ##: so they are left intact by cpp.

2005-09-12  Sergey Poznyakoff  <gray@gnu.org.ua>

	* elisp/links-mode.el: Produce valid XHTML 1.0 output
	* etc/links.tmpl: Likewise.

	* ppp/start-ppp: Renamed to ...
	* ppp/start-ppp.sh: ... this
	* ppp/.cvsignore: Updated
	* ppp/Makefile.am: Bugfixes
	* ckaliases/Makefile.am: Install ckaliases into sbin
	* consoleconf/Makefile.am (EXTRA_DIST): Remove
	* consoleconf/data/Makefile.am: Updated
	* doc/gsc.texi (ppp): Documented
	* etc/Makefile.am (EXTRA_DIST): Add links.tmpl

2005-09-11  Sergey Poznyakoff  <gray@gnu.org.ua>

	* README: Updated
	* doc/gsc.texi: Updated

	* README: Rewritten

	* doc/gsc.texi (consoleconf): Documented
	
	* consoleconf/data/dk: Renamed to ...
	* consoleconf/data/da: ... this
	* consoleconf/data/dk-latin1: Renamed to ...
	* consoleconf/data/da-latin1: ... this
	* consoleconf/data/gr: Renamed to ...
	* consoleconf/data/el: ... this 
	* consoleconf/README: Removed

2005-09-10  Sergey Poznyakoff  <gray@gnu.org.ua>

	* doc/gsc.texi (rc.firewall,dict-setup): Documented
	* configure.ac: Autodetect sendmail cf directory

2005-09-09  Sergey Poznyakoff  <gray@gnu.org.ua>

	* doc/gsc.texi: Updated
	* firewall/firewall.m4: Provide a default definition of SYSCONFDIR
	* elisp/links-mode.el (get-token): Do not relay on syntax tables
	to match whitespace. Use [ \t] instead.
	* etc/links.txt: More glagolitic sites.

2005-09-08  Sergey Poznyakoff  <gray@gnu.org.ua>

	* cvs/mksnapshot (module): Purge old snapshots. The number of
	snapshots to keep is set by MAXSNAPSHOTS variable. The default is
	3.
	* doc/gsc.texi: Updated

2005-09-07  Sergey Poznyakoff  <gray@gnu.org.ua>

	* firewall/firewall.m4: Act in accordance with ACTION variable
	* firewall/sample.m4: Do not include firewall.m4. It should be
	done by rc.firewall
	* rc.d/rc.firewall: Use $1 to decide what to do

	* doc/gsc.texi: Updated
	* mc/relay2.mc: New file
	* mc/Makefile.am (MCFILES): Add relay2.mc

2005-09-06  Sergey Poznyakoff  <gray@gnu.org.ua>

	* doc/gsc.texi: Updated
	* rc.d/rc.ipacct: Accept reload as an alias for reconfigure
	* rc.d/rc.local: Allow for absolute module names
	* rc.d/rc.ntpd: Minor improvements

2005-09-03  Sergey Poznyakoff  <gray@gnu.org.ua>

	* ckaliases/ckaliases.c: Add copyleft header
	* ckaliases/ckaliases.h: Likewise
	* ckaliases/gram.y: Likewise
	* ckaliases/lex.l: Likewise

	* doc/gsc.texi: Document ckaliases

	* Makefile.am, configure.ac, aspell/Makefile.am, bind/Makefile.am,
	bind/master/Makefile.am, bind/master/bin/Makefile.am,
	bind/slave/Makefile.am, bind/slave/bin/Makefile.am,
	ckaliases/Makefile.am, consoleconf/Makefile.am,
	consoleconf/bin/Makefile.am, consoleconf/data/Makefile.am,
	consoleconf/data/Makefile.am, consoleconf/data/motd/Makefile.am,
	cvs/Makefile.am, doc/Makefile.am, doc/gendocs_template,
	elisp/Makefile.am, etc/Makefile.am, firewall/Makefile.am,
	fixnamespace/Makefile, fsf-move/Makefile.am, mc/Makefile.am,
	ppp/Makefile.am, rc.d/Makefile.am, aspell/.cvsignore,
	bind/master/bin/.cvsignore, bind/master/.cvsignore,
	bind/slave/bin/.cvsignore, bind/slave/.cvsignore, bind/.cvsignore,
	ckaliases/.cvsignore, consoleconf/bin/.cvsignore,
	consoleconf/data/motd/.cvsignore, consoleconf/data/.cvsignore,
	consoleconf/.cvsignore, cvs/.cvsignore, elisp/.cvsignore,
	firewall/.cvsignore, ppp/.cvsignore, rc.d/.cvsignore,
	fsf-move/.cvsignore, doc/.cvsignore, etc/.cvsignore,
	fixnamespace/.cvsignore, mc/.cvsignore, .cvsignore: Added
	configuration framework 

	* consoleconf/bin/consoleconf: Moved ...
	* consoleconf/bin/consoleconf.sh: ... here
	
	* NEWS: New file
	* AUTHORS: Updated email
	* ckaliases/Makefile, cvs/Makefile, mc/Makefile: Removed
	* ckaliases/ckaliases.c: Include config.h
	* cvs/sv_sync_www.c: Likewise
	* ckaliases/lex.l: y.tab.c renamed to gram.c

	* doc/gsc.texi: Minor fixes.

2005-08-27  Sergey Poznyakoff  <gray@gnu.org.ua>

	* rc.d/rc.ppp: New file
	* rc.d/rc.local: Allow to start scripts with different user privs.

	* cvs/sv_sync_www.c: New option -c
	* doc/gsc.texi: Updated
	
	* mc: New directory
	* mc/Makefile: New file
	* mc/relay1.mc: New file
	* mc/trurl.mc: New file
	* mc/ulysses.mc: New file

2005-08-26  Sergey Poznyakoff  <gray@gnu.org.ua>

	* fixnamespace/fixnamespace: Implement debug mode
	Pass options to configure and make
	* fixnamespace/fixnamespace.el (tags-replace): Use `from' as it
	was passed. Set search to case sensitive and add _ to word
	constituents in both tags-loop-scan and tags-loop-operate
	(global-tag-replace): Pass 'words to regexp-opt
	(fixnamespace-loop-continue): Process #line directives.

	* fsf-move/fsf-move: Implement recent solutions from fixnamespace.

2005-08-25  Sergey Poznyakoff  <gray@gnu.org.ua>

	* doc: Added to the repository
	* doc/fdl.texi: Added to the repository
	* doc/gsc.texi: Added to the repository
	* doc/rendition.texi: Added to the repository
	
	* fixnamespace/fixnamespace: Check tar version
	
	* fixnamespace/fixnamespace: Fix recognition of emacs (or etags)
	version number.
	Use ETAGFLAGS to pass additional options to etags
	* fixnamespace/fixnamespace.el (fixnamespace-list-tags): Minor fix.

	* fixnamespace/fixnamespace: Check for necessary tools and a valid
	current working directory;
	Run make before make distcheck;
	Try to guess correct etags flags based on the Emacs version number.
	* fixnamespace/fixnamespace.el (tags-loop-operate): Declare _ as a
	part of word.

2005-08-24  Sergey Poznyakoff  <gray@gnu.org.ua>

	* fixnamespace/fixnamespace: New option -x
	* fixnamespace/fixnamespace.el: Likewise
	(tags-replace): Use let*

	* fixnamespace: New directory
	* fixnamespace/fixnamespace: New file
	* fixnamespace/fixnamespace.el: New file
	* README: Updated
	
2005-08-19  Sergey Poznyakoff  <gray@gnu.org.ua>

	* cvs/sv_sync_www.c: New option -m allows to specify access method
	for the repository.

2005-08-18  Sergey Poznyakoff  <gray@gnu.org.ua>

	* cvs/sv_sync_www.c (main): When synchronizing after initial
	commit, do not append local directory to cvsroot.
	* cvs/sync_www_flush: Fixed counting of updates

2005-08-17  Sergey Poznyakoff  <gray@gnu.org.ua>

	* cvs/sv_sync_www.c: Updated heading comment
	* cvs/sv_sync_www_flush: Pass CVSROOT to sv_sync_www.
	* cvs/sv_sync_www_schedule: Save three arguments to the spool.

2005-08-10  Sergey Poznyakoff  <gray@gnu.org.ua>

	* elisp/dot.emacs: Updated
	* etc/links.txt: Updated
	* etc/links.tmpl: Updated

2005-08-04  Sergey Poznyakoff  <gray@gnu.org.ua>

	* etc/links.txt: Updated

2005-08-01  Sergey Poznyakoff  <gray@gnu.org.ua>

	* README: Updated
	* cvs/sv_sync_www_flush: New file

2005-07-31  Sergey Poznyakoff  <gray@gnu.org.ua>

	* README: Updated
	* cvs/sv_logger: New file
	* cvs/sv_sync_www.c: New file
	* cvs/sv_sync_www_schedule: New file
	* cvs/.cvsignore: New file

	* COPYING,aspell/dict-setup,bind/master/bin/ndc.awk,
	consoleconf/README,consoleconf/bin/consoleconf,cvs/mksnapshot,
	elisp/dot.emacs,elisp/po.el,firewall/firewall.m4,firewall/sample.m4,
	ppp/pppscript.m4,ppp/start-ppp,rc.d/rc.firewall,rc.d/rc.inet1,
	rc.d/rc.ipacct,rc.d/rc.ntpd,rc.d/rc.tagr: Updated FSF postal mail
	address 

2005-07-04  Sergey Poznyakoff  <gray@Mirddin.farlep.net>

	* README: Updated
	* aspell/dict-setup: Fixed syntax of POSIX arithmetical
	expressions
	* ppp/start-ppp: Likewise
	* rc.d/rc.ntpd: Likewise.
	* elisp/links-mode.el: First build an abstract tree, then convert
	it to output format.
	* etc/links.tmpl: Synchronized with my current bookmark page
	* etc/links.txt: Likewise

2005-07-04  Sergey Poznyakoff  <gray@Noldor.runasimi.org>

	* elisp/links-mode.el: New file
	* etc: New directory
	* etc/links.txt: New file
	* etc/links.tmpl: New file

2005-06-26  Sergey Poznyakoff  <gray@Noldor.runasimi.org>

	* fsf-move/fsf-move: Remove -s option from sed invocation

2005-05-17  Sergey Poznyakoff  <gray@Mirddin.farlep.net>

	* fsf-move: New directory
	* fsf-move/fsf-move: New file
	* fsf-move/fsf-move.awk: New file
	* README (Contents): Updated

2005-04-30  Sergey Poznyakoff  <gray@Ulysses.farlep.net>

	* cvs/mksnapshot (module): Assume $1 as the name of the directory
	to be archived.
	(main_savane): Change to $WD before processing

2005-04-29  Sergey Poznyakoff  <gray@Ulysses.farlep.net>

	* ckaliases/ckaliases.c (mark_connected): Prevent coredump.

2005-04-28  Sergey Poznyakoff  <gray@Mirddin.farlep.net>

	* cvs/mksnapshot: Updated to use with Savannah
	* ckaliases: New directory
	* ckaliases/Makefile: New file
	* ckaliases/ckaliases.c: New file
	* ckaliases/ckaliases.h: New file
	* ckaliases/gram.y: New file
	* ckaliases/lex.l: New file

2005-04-14  Sergey Poznyakoff  <gray@Mirddin.farlep.net>

	* aspell/: New directory
	* aspell/dict-setup: New file. Download and setup aspell
	dictionaries for a given set of languages.

	* bind/: New directory
	* bind/master/:	New directory
	* bind/master/bin/: New directory
	* master/bin/ndc.awk: New file
	* master/bin/ndconf: New file
	* master/Distfile: New file
	* slave/bin/buildconf: New file
	
	* README: Updated

2005-03-24  Sergey Poznyakoff  <gray@Ulysses.farlep.net>

	* cvs/mksnapshot (module): Exclude CVS files from produced archives

2005-03-23  Sergey Poznyakoff  <gray@Ulysses.farlep.net>

	* cvs/mksnapshot: Create snapshot tarballs from the user's
	CVS repository
	* firewall/firewall.m4: M4 wrappers for setting firewalls
	* firewall/sample.m4: Sample /etc/firewall/rules.m4 file
	* rc.d/rc.firewall: Setup firewalls

2005-03-16  Sergey Poznyakoff  <gray@Mirddin.farlep.net>

	* elisp: New directory
	* elisp/dot.emacs: New file
	* elisp/emacs.el: New file
	* elisp/po.el: New file
	* elisp/site-start.el: New file
	* elisp/spell-check.el: New file

2005-03-01  Sergey Poznyakoff  <gray@Noldor.runasimi.org>

	* consoleconf/bin/consoleconf: Minor improvements. Use setxkbmap
	under X11. Use eval to execute commands.
	* consoleconf/data/ru: Add X11

	* README: Updated
	* consoleconf/: Added to the repository
	* consoleconf/README: Likewise
	* consoleconf/bin:
	* consoleconf/bin/consoleconf:
	* consoleconf/data/:
	* consoleconf/data/-latin1:
	* consoleconf/data/def:
	* consoleconf/data/dk:
	* consoleconf/data/dk-latin1:
	* consoleconf/data/es:
	* consoleconf/data/gr:
	* consoleconf/data/no:
	* consoleconf/data/no-latin1:
	* consoleconf/data/ru:
	* consoleconf/data/pl:
	* consoleconf/data/uk:
	* consoleconf/data/motd/:
	* consoleconf/data/motd/no-latin1:
	* consoleconf/data/X11/:

2005-01-31  Sergey Poznyakoff  <gray@Noldor.runasimi.org>

	* ppp/login: New file
	* ppp/modem: New file
	* ppp/numbers: New file
	* ppp/pppscript.m4: New file
	* ppp/start-ppp: New file
	* README: Updated

2005-01-28  Sergey Poznyakoff  <gray@Mirddin.farlep.net>

	* rc.d/rc.autofs (getmounts): Place daemon arguments immediately
	after the command name.
	(status, case reload): Compress whitespace in getmounts output

2005-01-25  Sergey Poznyakoff  <gray@Mirddin.farlep.net>

	* README: Updated
	* rc.d/rc.ntpd: New file
	* rc.d/rc.autofs: New file
	* rc.d/rc.local: New file
	* rc.d/rc.ipacct: New file
	* rc.d/rc.tagr: New file
	
2001-05-18  Sergey Poznyakoff  <gray@Mirddin.farlep.net>
	
	Initial checkin


Local Variables:
mode: change-log
version-control: never
End:
