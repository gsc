;;;; fixnamespace.el - Fix project's namespace
;; Copyright (C) 2005 Sergey Poznyakoff

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
;; MA 02110-1301, USA.

;;;; The purpose of this program is to fix a namespace of exported symbols
;;;; of a project by prepending a prefix to each symbol.
;;;;
;;;; Usage: emacs -batch -l fixnamespace.el [-x S] PREFIX TABLE FILE [FILE...]
;;;; where:
;;;;   PREFIX is the prefix to prepend to all global symbols
;;;;   TABLE  is the TAGS table to use (or a directory where it resides)
;;;;   FILE   is the header file(s) from which to obtain a list of global
;;;;          symbols (only names, no directories)
;;;; options:
;;;;   -x S   exclude symbol S from replacing. The option can appear multiple
;;;;          times
;;;;
;;;; The program collects global symbols from header FILEs, and prepends
;;;; PREFIX to every occurrrence of these in project source files.
;;;;
;;;; Prerequisites: Valid TAGS tables must exist in each project subdirectory.
;;;; The program itself should be started from the projects' top-srcdir.
;;;;
;;;; Sample usage: emacs -batch -l fixnamespace.el mu_ . messages.h
;;;; 
;;;; Do not run this program directly, unless you know what you are doing.
;;;; Instead, use wrapper script `fixnamespace' in this directory. 
;;;; 

(require 'etags)
(require 'cl)

(defvar exclude-pattern)
(defvar processed-file-list nil)
	
(defun fixnamespace-loop-continue (&optional first-time)
  (let (new
	;; Non-nil means we have finished one file
	;; and should not scan it again.
	file-finished
	original-point
	(messaged nil))
    (catch 'done
      (while
	  (progn
	    ;; Scan files quickly for the first or next interesting one.
	    ;; This starts at point in the current buffer.
	    (while (or first-time
		       file-finished
		       (save-restriction
			 (widen)
			 (not (tags-loop-eval tags-loop-scan))))

	      ;; If nothing was found in the previous file, and
	      ;; that file isn't in a temp buffer, restore point to
	      ;; where it was.
	      (when original-point
		(goto-char original-point))

	      (if (and (not first-time) (null next-file-list))
		  (throw 'done nil))

	      (setq file-finished nil)
	      (setq new (next-file first-time t))
	      (when new
		(push new processed-file-list))
	      
	      ;; If NEW is non-nil, we got a temp buffer,
	      ;; and NEW is the file name.
	      (when (or messaged
			(and (not first-time)
			     (> baud-rate search-slow-speed)
			     (setq messaged t)))
		(message "Scanning file %s..." (or new buffer-file-name)))
	      
	      (setq first-time nil)
	      (setq original-point (if new nil (point)))
	      (goto-char (point-min)))
	    
	    ;; If we visited it in a temp buffer, visit it now for real.
	    (if new
		(let ((pos (point)))
		  (erase-buffer)
		  (set-buffer (find-file-noselect new))
		  (setq new nil)		;No longer in a temp buffer.
		  (widen)
		  (goto-char pos))
	      (push-mark original-point t))
	    
	    (switch-to-buffer (current-buffer))
	    (goto-char (point-min))
	    (while (re-search-forward "#line\\s +[0-9]+\\s +\"\\(.*\\)\""
				      nil t)
	      (let ((name (match-string 1)))
		(let ((full-name (concat
				  (file-name-directory
				   (car processed-file-list))
				  name)))
		  (unless (or (member full-name next-file-list)
			      (member full-name processed-file-list))
		    (push full-name next-file-list)))))
	    (goto-char (point-min))
	      

	    ;; Now operate on the file.
	    ;; If value is non-nil, continue to scan the next file.
	    (tags-loop-eval tags-loop-operate))
	(setq file-finished t))
      t)))

(defun tags-replace (from to &optional first)
  (let  ((tags-loop-scan (list 'progn
			       '(modify-syntax-entry ?_ "w")
			       '(setq case-fold-search nil)

			       (list 'prog1
				     (list 'if (list 're-search-forward
						     (list 'quote from) nil t)
					   ;; When we find a match, move back
					   ;; to the beginning of it so 
					   ;; re-search-forward will see it.
					   '(goto-char (match-beginning 0))))))
	 (tags-loop-operate (list 'progn
				  '(modify-syntax-entry ?_ "w")
				  '(setq case-fold-search nil)
				  (list 'while
					(list 're-search-forward
					      (list 'quote from) nil t)
					(list 'replace-match
					      (list 'quote to) nil nil))
				  (list 'save-buffer)
				  nil)))
    (while (fixnamespace-loop-continue (and first
					    (progn (setq first nil) t))))))

(defun fixnamespace-list-tags (file regex tag-list)
  (modify-syntax-entry ?_ "w")
  (goto-char 1)
  (when (if file
	    (search-forward (concat "\f\n" file ",") nil t)
	  (search-forward-regexp "\f\n\\([^\\s ]+\\)," nil t))
    (forward-line 1)
    (while (not (or (eobp) (looking-at "\f")))
      (let ((tag (buffer-substring (point)
				   (progn (skip-chars-forward "^\177")
					  (point)))))
	
	(unless (string-match "#\\s *define" tag)
	  (when (looking-at "[^\n]+\001")
	    ;; There is an explicit tag name; use that.
	    (setq tag (buffer-substring (1+ (point)) ; skip \177
					(progn (skip-chars-forward "^\001")
					       (point)))))
	  
	  (unless (or (string-match "^__.*__$" tag)
		      (string-match exclude-pattern tag))
	    (when (string-match "\\(\\w+\\)\\s *[(;,]?\\s *$" tag)
	      (setq tag (match-string 1 tag))
	      (if (not (string-match regex tag))
		  (push tag tag-list))))))
      (forward-line 1)))
  tag-list)

(defun collect-tags-from-file (file regex &optional next-match)
  (save-excursion
    (let ((first-time t)
	  (tag-list nil))
      (while (visit-tags-table-buffer (not first-time))
	(setq first-time nil
	      tag-list (fixnamespace-list-tags file regex tag-list)))
      tag-list)))

(defun collect-tags (regex table-name file)
  "Collect from source FILE, visiting TABLE-NAME, all tags that do not
match REGEX. TABLE-NAME is passed to `visit-tags-table-buffer' (which see)"
  (let ((tags-add-tables nil))
    (visit-tags-table table-name)
    (tags-table-check-computed-list)
    (if (and source-file (listp file))
	(mapcan
	 (function (lambda (x)
		     (collect-tags-from-file x regex)))
	 file)
      (collect-tags-from-file file regex))))

;; Sample usage:
;; (global-tag-replace "mu_" "include" "message.h" '("mail" "include"))    

(defun global-tag-replace (prefix &optional start-table source-file file-list)
  (let ((tags-table-list (or file-list '(".")))
	(tags-file-name nil)
	(start-table (or start-table "."))
	(prefix-re (concat "^" prefix ".*")))
    (let ((regex (funcall 'regexp-opt
			  (collect-tags prefix-re
					start-table source-file)
			  'words)))
      (tags-replace regex (concat prefix "\\&") t))))
     
;;;; Main
(setq max-specpdl-size 2048)

(let ((exclude-pattern-list))

  (while (string= (car command-line-args-left) "-x")
    (let ((tail (cdr command-line-args-left)))
      (push (car tail) exclude-pattern-list)
      (setq command-line-args-left (cdr tail))))

  (setq exclude-pattern (funcall 'regexp-opt
				 exclude-pattern-list
				 'words))

  (let ((arg-count (length command-line-args-left)))
    (if (>= arg-count 3)
	(eval (append (list 'global-tag-replace)
		      (mapcar
		       (function (lambda (x)
				   (cond
				    ((and (stringp x) (string= x "nil"))
				     nil)
				    ((listp x)
				     (list 'quote x))
				    (t
				     x))))
		       (if (= arg-count 3)
			   command-line-args-left
			 (list (car command-line-args-left)
			       (cadr command-line-args-left)
			       (nthcdr 2 command-line-args-left))))))
      (error "Not enough arguments"))))  
  
;;;; End of fixnamespace.el
