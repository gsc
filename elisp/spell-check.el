;;; Spell-checker settings
(setq ispell-program-name "aspell")
(require 'ispell)

(setq my-ispell-alist
      '(("catalan" "" "" "" nil ("-B" "-d" "catalan") "~tex" utf-8)
	("espa~nol"
	 "[A-Z\301\311\315\321\323\332\334a-z\341\351\355\361\363\372\374]" "[^A-Z\301\311\315\321\323\332\334a-z\341\351\355\361\363\372\374]"
	 "[---]" nil ("-B" "-d" "espa~nol") "~tex" iso-8859-1)
	("espanol" "" "" "" nil ("-d" "espanol") nil utf-8)
	("ukrainian" "" "" "" nil ("-d" "ukrainian") nil utf-8)
	("greek" "" "" "[---]" nil ("-B" "-d" "el") "~tex" utf-8)))

(setq ispell-dictionary-alist (append my-ispell-alist
				      ispell-dictionary-alist))
