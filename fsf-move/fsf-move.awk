# fsf-move.awk - Change FSF postal address or GPL version in a source file
# Copyright (C) 2005, 2007 Sergey Poznyakoff
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program. If not, see <http://www.gnu.org/licenses/>.

BEGIN {
  cur_year = strftime("%Y", systime())
  PREFIX_RE = "[ \t]*\\*+"
  EXITCODE=0
  if (PATFILE) {
    patcount = 0
    i = 0
    while ((getline < PATFILE) > 0) {
      if (i % 2)
        replace[patcount++] = $0
      else
        pattern[patcount] = $0
      i++
    }
  }
}

function run_replace(s,     i, n)
{
  n = 0
  for (i = 0; i < patcount; i++) 
    n += sub(pattern[i], replace[i], s);
  return n ? s : "";
}

function debug(s) {
## For debugging, uncomment this
#  print "DBG: " s > "/dev/stderr"
}

## For a very extensive debugging, uncomment this:
#{ print state ": " $0 > "/dev/stderr" }

CMODE && state == 0 && /^[ \t]*\/\*.*\*\// {
  newstr = run_replace($0)
  print newstr ? newstr : $0
  next
}

CMODE && state == 0 && /^\/\*/ {
  state = 1;
  check_prefix = 1;
  sub("^\\/\\*", "", $0);
  line_cnt = 1;
  debug("A: " line_cnt " " $0)
  line[line_cnt++] = $0;
  accline = $0;
  next
}

function format_out(line,          fmt_cmd,command) {
    fmt_cmd = "fmt -u -w " 79 - prefix_length
    debug("FORMATTING " line)
    debug("WITH " fmt_cmd)
    if (prefix_length) 
#      command = "tr -s ' ' | " fmt_cmd " |sed 's/^.*/" prefix "&/'"
      command = fmt_cmd " |sed 's/^.*/" prefix "&/'"
    else
      command = fmt_cmd
    print line | command
    close(command)
}	

function update_copyright_year(line,len,        updated,n,a,i,y,s) {
  updated = 0;
  gsub(/, */,",",line)
  n = split(substr(line, len), a, ",");
  for (i = 1; i <= n; i++) {
    if (match(a[i], "19[0-9][0-9] *") || match(a[i], "200[0-9] *")) {
      y = substr(a[i],1,4)
      if (y == cur_year) {
	updated = 1
      }
    } else {
      i--
      break
    }
  }
  if (!updated) 
    a[i] = substr(a[i],1,4) ", " cur_year  substr(a[i],5);
  s = substr(line, 1, len-1);
  if (i >= 1)
    s = s " " a[1]
  for (i = 2; i <= n; i++) 
    s = s ", " a[i]
#  print "AFTER " s 	    
  return s
}

function flush_line(        newstr) {
  debug("FLUSHING LINE")
  if (state == 1) {
    printf "/*"
    state = 2
  }
  if (newstr = run_replace(accline)) {
    format_out(newstr)
  } else if (match(accline, /Copyright *\(C\)  */)) {
    format_out(update_copyright_year(accline, RSTART+RLENGTH))	    
  } else {
    for (i = 1; i < line_cnt; i++) 
      print line[i]
  }
  line_cnt = 1;
  accline = "";
}

state > 0 && state < 3 && check_prefix {
  if (!match($0, /\*\//) && match($0, "^" PREFIX_RE)) {
    prefix_length = RLENGTH
    prefix = substr($0, 1, RLENGTH);
    debug("PREFIX " prefix)
    debug("OBTAINED FROM " $0)
    prefix_repl = "" # sprintf("%*.*s", RLENGTH, RLENGTH, "")
    check_prefix = 0;
  }
}

state > 0 && state < 3 && NF == 0 {
  flush_line();
  print 
  next
}

state != 3 && /Copyright *\(C\)  */ {
  flush_line();
}

state > 0 && state < 3 {
  if (sub("\\*\\/[ \t]*$", "")) 
    flush_now = 1;
  else
    flush_now = 0;

  if (prefix_length && prefix == substr($0, 1, prefix_length)) 
    s = prefix_repl substr($0, prefix_length+1);
  else
    s = $0;

  if (match(s, "^[ \t]*$")) {
    flush_line();
    if (flush_now) {
      s = s "*/"
      state = 0;	
    } else
      s = prefix s
    print s
    next  
  }

  if (flush_now) {
    s = s "*/"
    $0 = $0 "*/"
  }
  debug("A: " line_cnt " " s)
  line[line_cnt++] = $0;
  accline = accline s;
  if (flush_now) {
    debug("BEFORE " line_cnt " " accline)
    flush_line();
    debug("AFTER " line_cnt)
    state = 0;
  }
  next
}

function acc_c_string(s,  n) {
  n = gsub("\\\\n", "\n", s)
  n += gsub("\\\\t", "\t", s)
  n += gsub("\\\\f", "\f", s)
  if (length(s) == n)
    return 1
  else {    
    accline = accline s
    return 0
  }
}

CMODE && state == 0 && /^#/ {
  print
  next
}

CMODE && state == 0 && /"[^"]+"[ \t]*$/ {
  if (match($0, "\"[^\"]+\"")) {
    printf "%s", substr($0, 1, RSTART-1);
    state = 3
    s = substr($0, RSTART+1, RLENGTH-2);
    line_cnt=1
    debug("B: " line_cnt " " s)
    line[line_cnt++] = substr($0, RSTART)
    accline = ""
    acc_c_string(s)
    next
  } else {
    print ARGV[ARGIND] ":" NR ": ERROR" > "/dev/stderr"
    EXITCODE=1
  }
}

function flush_c_line(s,   newstr) {
  if (newstr = run_replace(accline)) {
    fmt_cmd = "fmt -c"
    print newstr > "/tmp/fsf-move"
    fflush("/tmp/fsf-move")
    command = "cat /tmp/fsf-move | " fmt_cmd 
    while ((command | getline VAR) > 0) {
        gsub("\t", "\\\\t", s)
        gsub("\f", "\\\\f", s)        
        print prefix_repl "\"" VAR "\\n\"" 
    }
    if (s)
      print s;
    close(command)
  } else {
    for (i = 1; i < line_cnt; i++)
        print line[i]
  }
  line_cnt = 1;
  accline = "";
}

state == 3 && /[ \t]*"[^"]+"[ \t]*$/ {
  if (match($0, "\"[^\"]+\"")) {
     s = substr($0, RSTART+1, RLENGTH-2);
     if (acc_c_string(s) == 1) {
       prefix_repl = sprintf("%*.*s", RSTART-1, RSTART-1, ""); 
       flush_c_line();
       print 
     } else
       line[line_cnt++] = $0
     next
  }
}

state == 3 && /[ \t]*"[^"]+"[ \t]*[);]/ {
  if (match($0, "\"[^\"]+\"")) {
     s = substr($0, RSTART+1, RLENGTH-2);
     acc_c_string(s) 
     line[line_cnt++] = $0
     prefix_repl = sprintf("%*.*s", RSTART-1, RSTART-1, "");
     flush_c_line(substr($0, RSTART+RLENGTH));
  } else {
     print NR  ": UNEXPECTED FAILURE " $0 > "/dev/stderr"
     EXITCODE=1
  }
  state = 0
  next
}

state == 3 {
   flush_c_line()
   state = 0
#  print ARGV[ARGIND] ":" NR ": ERROR: Missing `\"'?" > "/dev/stderr"
#  EXITCODE=1
}

# Single-line comments

function iscomment(s) {
  return COMMENT_RE && match(s, "^" COMMENT_RE);
}

!CMODE && state == 0 && iscomment($0) {
  prefix_length = RLENGTH
  prefix = substr($0, 1, RLENGTH);
  debug("PREFIX " prefix)
  prefix_repl = sprintf("%*.*s", RLENGTH, RLENGTH, "")

  line_cnt = 1   
  debug("C: " line_cnt " " $0)
  line[line_cnt++] = $0
  s = substr($0, prefix_length+1);
  accline = s
  state = 4
  next
}

state == 4 && iscomment($0) {
  if (prefix != substr($0, 1, RLENGTH)) {
    flush_line()
    print
  } else {
    s = substr($0, prefix_length+1);
    if (match(s, "^[ \t]*$")) {
      flush_line()
      print 
    } else {
      debug("C: " line_cnt " " $0)
      line[line_cnt++] = $0
      accline = accline s
    }
  }
  next
}

state == 4 {
  flush_line()
  state = 0
}

state == 0 { print }

END {
  if ((state > 0 && state < 3) || state == 4)
    flush_line();
  else if (state == 3)
    flush_c_line();
  exit EXITCODE
}

# End of fsf-move.awk

  


