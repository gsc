#                                               -*- Autoconf -*-
# This file is part of GSC
# Copyright (C) 2005, 2006, 2007 Sergey Poznyakoff
#
# GSC is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# GSC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GSC.  If not, see <http://www.gnu.org/licenses/>.

AC_PREREQ(2.59)
AC_INIT([gsc], 1.1, [gray@gnu.org.ua])
AC_CONFIG_SRCDIR([cvs/sv_sync_www.c])
AC_CONFIG_AUX_DIR([build-aux])
AC_CONFIG_HEADER([config.h])
AM_INIT_AUTOMAKE
	
# Checks for programs.
AC_PROG_CC
gl_EARLY	
AC_PROG_LEX
AC_PROG_YACC
AC_PROG_RANLIB

# Checks for libraries.

# Checks for header files.
AC_HEADER_STDC
AC_HEADER_SYS_WAIT
AC_CHECK_HEADERS([stdlib.h string.h sys/file.h unistd.h sys/sendfile.h])

# Checks for typedefs, structures, and compiler characteristics.
AC_C_CONST
AC_TYPE_SIGNAL	
AC_TYPE_UID_T
AC_TYPE_PID_T
AC_TYPE_SIZE_T
AC_HEADER_STDBOOL

# Checks for library functions.
gl_INIT  
AC_FUNC_FORK
AC_FUNC_MALLOC
AC_FUNC_MEMCMP
AC_FUNC_STAT
AC_FUNC_VPRINTF
AC_CHECK_FUNCS([memset strchr strdup strerror strrchr setegid setregid setresgid seteuid setreuid vsyslog sysconf getdtablesize])

# *************************
# Sendmail version or cfdir
# *************************
AC_SUBST(SENDMAIL_VERSION)
AC_ARG_WITH([sendmail-version],
	    [AC_HELP_STRING([--with-sendmail-version=VERSION],
	                    [Build .cf files for given Sendmail version])],
            [SENDMAIL_VERSION=$withval],
	    [SENDMAIL_VERSION=])

AC_SUBST(SENDMAIL_CFDIR)	    
AC_ARG_WITH([sendmail-cfdir],
	    [AC_HELP_STRING([--with-sendmail-cfdir=DIR],
	                    [Specify full name of Sendmail cf directory (e.g.: /usr/src/sendmail-8.13.1/cf/cf). Overrides --with-sendmail-version])],
	    [SENDMAIL_CFDIR=$withval],
	    [SENDMAIL_CFDIR=])

AC_MSG_CHECKING([for Sendmail cf directory])
if test -n "$SENDMAIL_CFDIR"; then
  AC_MSG_RESULT([$SENDMAIL_CFDIR])	
elif test -n "$SENDMAIL_VERSION"; then     
  AC_MSG_RESULT([none, use version $SENDMAIL_VERSION])	   
else
  SENDMAIL_CFDIR=`find /usr/src/ /usr/local/src/ -name sendmail-* -type d -maxdepth 1 | 
    sed 's/.*sendmail-\(.*\)/\1 &/' | 
    sort +0 -1 -r | 
    while read VERSION DIR 
    do
      if test -r $DIR/cf/cf/Makefile; then
        echo "$DIR"
	break
      fi
    done`
  AC_MSG_RESULT([${SENDMAIL_CFDIR:-none}])
fi   	    		    	    			        

# ***********************************
# Check for Emacs site-lisp directory
# ***********************************
AM_PATH_LISPDIR

if test "$EMACS" != "no"; then
	lisp_LISP='$(LISPSRC)'
fi
AC_SUBST(lisp_LISP)

# ***********************
# Module list
# ***********************
MODLIST='aspell
bind
ckaliases
consoleconf
cvs
doc
etc
elisp
firewall
fixnamespace
fsf-move
maint
mc
ppp
rc.d'

AC_ARG_VAR([XMODLIST],
        [Comma-delimited list of modules to exclude from building (see also --enable-modules)])
SENSE=

AC_ARG_ENABLE([module],
              [AC_HELP_STRING([--enable-modules],
                              [Enable only modules from XMODLIST])],
              [case $enableval in
               yes) SENSE=include;;
               no)  SENSE=exclude;;
               *)   AC_MSG_FAILURE([Invalid argument for --enable-modules])
               esac],
              [SENSE=exclude])

if test "$SENSE" = "include"; then
  MODLIST=`echo $XMODLIST | tr ',' '\n' | sort`
else
  MODLIST=`echo "${MODLIST},$XMODLIST" | tr ',' '\n' | sort | uniq -u`
fi

# **********************
# Perl  
# **********************
AC_PATH_PROG(PERL, perl)

# **********************	
# Consoleconf variables
# **********************	
AC_SUBST(CONSOLECONFDIR)
CONSOLECONFDIR='${datadir}/consoleconf'
AC_SUBST(CONSOLEFONTSDIR)
CONSOLEFONTSDIR=/usr/share/kbd/consolefonts
AC_SUBST(CONSOLETRANSDIR)
CONSOLETRANSDIR=/usr/share/kbd/consoletrans

AH_BOTTOM([
#if __GNUC__ < 2 || (__GNUC__ == 2 && __GNUC_MINOR__ < 7)
#  define __attribute__(x)
#endif

#ifndef GSC_PRINTFLIKE
# define GSC_PRINTFLIKE(fmt,narg) __attribute__ ((__format__ (__printf__, fmt, narg)))
#endif
])
	
AC_CONFIG_FILES([Makefile
                 aspell/Makefile
                 bind/Makefile
                 bind/master/Makefile
                 bind/master/bin/Makefile
		 bind/slave/Makefile
		 bind/slave/bin/Makefile
                 ckaliases/Makefile
                 consoleconf/Makefile
                 consoleconf/bin/Makefile
                 consoleconf/data/Makefile
                 consoleconf/data/motd/Makefile
                 cvs/Makefile
                 doc/Makefile
		 etc/Makefile
                 elisp/Makefile
                 firewall/Makefile
                 fixnamespace/Makefile
                 fsf-move/Makefile
		 gnu/Makefile
	         lib/Makefile
		 maint/Makefile
                 ppp/Makefile
                 rc.d/Makefile])
		 
AC_OUTPUT
