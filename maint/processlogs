#! /bin/sh
# processlogs -- special processing for webalizer logs.
# Copyright 2006, 2008 Sergey Poznyakoff
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

PATH=$PATH:/usr/local/bin/:/usr/sbin
CONFDIR=/etc/webalizer.d
WEBALIZER=webalizer
FILELIST=
TMPDIR=/tmp
: ${AWK:=awk}
LOGDIR=/var/log/apache
: ${APACHECTL:=/usr/local/apache2/bin/apachectl}

RUN=
VERBOSE=0
LROPT=
SKIP_DNS_RESOLVE=

verbose() {
	if [ $VERBOSE -gt 0 ]; then
		echo $*
	fi
}

error() {
	code=$1
	shift
	echo $* >&2
	if [ $code -gt 0 ]; then
		exit $code
	fi
}

usage() {
	cat <<-EOT
		usage: $PROGNAME [OPTIONS] 

		OPTIONS are

		-h        Display this help list
		-v        Produce verbose output
		-n        Dry run. Print what would have be done without doing
		          anything. Implies -v.
		-d DIR    Set configuration directory (default $CONFDIR)
		-f FILE   Set file list file (default \$CONFDIR/FILES)
	EOT
}

PROGNAME=$0
while getopts "hvnl:d:f:s" OPTION
do
	case $OPTION in
	h)	usage
		exit 0;;
	v)	VERBOSE=$(($VERBOSE + 1));;
	n)	RUN=:
		LROPT=-d
		VERBOSE=$(($VERBOSE + 1));;
	d)	CONFDIR=$OPTARG;;
	f)	FILELIST=$OPTARG;;
	s)	SKIP_DNS_RESOLVE=yes
	esac
done

shift $(($OPTIND - 1))

while [ $# -ne 0 ]
do
	case $1 in
	*=*) eval $1;;
	*) error 1 "Unexpected argument";;
	esac
	shift
done

test -d $CONFDIR || error 1 "Configuration directory does not exist"
if [ -z "$FILELIST" ]; then
	FILELIST=$CONFDIR/FILES
fi	
test -r $FILELIST || error 1 "Cannot read file list"

if [ $VERBOSE -gt 0 ]; then
	LROPT="$LROPT -v"
fi

TMPLOGDIR=$TMPDIR/processlogs.$$
TMP=$TMPDIR/logrotate.$$.conf

$APACHECTL >/dev/null 2>&1
test $? -eq 127 && error 1 "$APACHECTL does not exist or is not executable"

cleanup() {
	rm -f $TMP
        if [ $VERBOSE -lt 2 ]; then
		test -d "$TMPLOGDIR" && rm -rf $TMPLOGDIR
	else
		verbose "Leaving processed logs in directory $TMPLOGDIR"
	fi   
}

trap 'cleanup' 1 2 13 15

read_filelist() {
	line=0
	while read logname confname filter
	do
		line=$(($line + 1))
		case "$logname-$confname" in
		\#*) ;;
		-)  ;;
		\[*\]-)
		    LOGDIR=`expr "$logname" : '\[\(.*\)\]'`
		    if [ ! -d $LOGDIR ]; then
			error 1 "$1:$line: Directory does not exist"
		    fi
		    ;;
		*-) error 0 "$1:$line: Missing confname";;
		*)  $2 "$logname" "$confname" "$filter"
		esac
	done < $1
}

collect_logs() {
        loglist="$loglist
$LOGDIR/$1"
}

handle_log() {
	dir=`sed -n 's/OutputDir[	]*\(.*\)/\1/p' $CONFDIR/$2`
	if [ -n "$dir" ]; then
		if [ ! -d "$dir" ]; then
			verbose "Creating output directory $dir"
			$RUN mkdir -p $dir
		fi
	fi
	filter="${3:-cat}"
	LOGFILE=$LOGDIR/$1
	verbose "Running $filter $LOGFILE >> $TMPLOGDIR/$2"
	eval "$filter $LOGFILE >> $TMPLOGDIR/$2"
	configlist="$configlist $2"
}

sortlog() {
 $AWK '
   BEGIN {
     month["Jan"] = 1
     month["Feb"] = 2
     month["Mar"] = 3
     month["Apr"] = 4
     month["May"] = 5
     month["Jun"] = 6
     month["Jul"] = 7
     month["Aug"] = 8
     month["Sep"] = 9
     month["Oct"] = 10
     month["Nov"] = 11
     month["Dec"] = 12
   }
   # [14/Feb/2008:05:11:47 +0200
   /.+ .+ .+ \[[0-9][0-9]?\/[A-Z][a-z][a-z]\/[0-9][0-9][0-9][0-9].*\]/ {
     date=substr($4,2) " " substr($5,1,length($5)-1)
     n = split(date, p, /[/:]/)
     ts = mktime(p[3] " " month[p[2]] " " p[1] " " p[4] " " p[5] " " p[6] " " p[7])
     print ts, $0
     next}
   # Tue Dec  7 06:13:34 2004  
   /[A-Z][a-z][a-z] [A-Z][a-z][a-z]  *[0-9][0-9]?/ {
     n = split($4, p, /:/)
     ts = mktime($5 " " month[$2] " " $3 " " p[1] " " p[2] " " p[3])
     print ts, $0 }' $1 |
     sort -n -k1,2 |
     cut -d ' ' -f2- 
}

loglist=
read_filelist $FILELIST collect_logs
loglist=`echo "$loglist" | sort | uniq | tr '\n' ' '`
verbose "Logfiles: $loglist"

if [ -z "$SKIP_DNS_RESOLVE" ]; then
	verbose "Updating DNS Cache"
	if [ -z "$RUN" ]; then
		(echo $loglist | xargs cat) | webazolver
	fi
fi

mkdir $TMPLOGDIR || exit 1
verbose "Processing individual logs"
configlist=
read_filelist $FILELIST handle_log
for file in $configlist
do
  cmd="sortlog $TMPLOGDIR/$file | $WEBALIZER -c $CONFDIR/$file"
  verbose "Running $cmd"
  if [ -z "$RUN" ]; then
    eval $cmd
  elif [ $VERBOSE -lt 2 ]; then
    sortlog $TMPLOGDIR/$file > $TMPLOGDIR/${file}.sorted
  else
    sortlog $TMPLOGDIR/$file > /dev/null
  fi
done

if [ -z "$SKIP_LOGROTATE" ]; then
  cat >$TMP <<EOT
monthly
rotate 6
create
compress

$loglist {
        sharedscripts
        postrotate
                $APACHECTL restart
        endscript
}
EOT

  if [ $VERBOSE -ge 2 ]; then
    echo "# Logrotate configuration file:"
    cat $TMP
    echo "# EOF"
  fi

  verbose "Running logrotate --state $CONFDIR/logrotate.status $LROPT $TMP"
  $RUN logrotate --state $CONFDIR/logrotate.status $LROPT $TMP
fi

cleanup
