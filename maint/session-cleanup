#! /bin/sh
# session-cleanup - Manage PHP session files
# Copyright (C) 2005, 2007 Sergey Poznyakoff
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

PROGNAME=`basename $0`
TTL=3
PHPCF=/etc/apache/php.ini
RM=
RMCMD=rm
XARGS_T=

error() {
    echo "{$PROGNAME}: " $* >&2
    exit 1
}

usage() {
    cat <<-EOF
	usage: $PROGNAME [OPTIONS] [CONFIG-FILE]
	$PROGNAME -- Manage PHP session files
		
	OPTIONS are:
	  -c FILE         Use FILE instead of $PHPCF
	  -h              Display this help summary
	  -n              Dry run. Only display which files will be removed
	  -t TTL          Set session TTL (default $TTL). Measured in days
	  -v              Verbose mode
EOF
}

read_config() {
    if [ -r $1 ]; then
        while read REGEX TTL
        do
            case $REGEX in
            \#|"")  ;;
            *)  FCMD="$FCMD -o \\( -exec grep -q '$REGEX' {} \\; -ctime +$TTL \\)"
            esac
        done < $1
    else
        error "Cannot read $1"
    fi
}

while getopts "c:hnt:v" OPTION
do
    case $OPTION in
    c) PHPCF=$OPTARG;;
    h) usage; exit 0;;
    n) RMCMD=echo;;
    t) TTL=$OPTARG;;
    v) XARGS_T=-t ;;
    *) error "Try $PROGNAME -h for more information." ;;
    esac
done

shift $(($OPTIND - 1))

FCMD="-daystart -ctime +$TTL"
RM=${RMCMD:-rm}
case $# in
0)	;;
1)	read_config $1;;
*)	usage
	exit 1
esac

# proc_sessions ACTION DIR [DEPTH]
proc_sessions() {
    if [ $# -eq 3 ]; then
	DEPTH="-maxdepth $(($3 + 1))"
    else
	DEPTH=
    fi
    (cd $2;
     eval find . -type f $DEPTH \\\( $FCMD \\\) -print | xargs -r $XARGS_T $1)
}


D=`sed -n 's/^[ \t]*session\.save_path[ \t*]=[ \t]*"\?\([^"]*\)"\?[ \t]*;/\1/p' $PHPCF`
export RM
case $D in
[0-9]\;*)  echo $D | sed 's/;/ /' |
           (read DEPTH DIR;
   	      proc_sessions "$RM" $DIR $DEPTH)
           ;;
*)         clean_sessions "$RM" $D
esac

# End of session-cleanup

								