#!/usr/bin/perl
# This file is part of the Savane project
# <http://gna.org/projects/savane/>
#
#  Copyright (C) 2005, 2006, 2007 Sergey Poznyakoff <gray--at--gnu.org>
#  Copyright 2004 (c) Mathieu Roy <yeupou--at--gnu.org> 
#
# The Savane project is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# The Savane project is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with the Savane project; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
#
#
#
# FIXME: check the drawback explanation below.

use strict;
use Getopt::Long;
use Savane;

use POSIX qw(strftime);

$ENV{PATH}="/usr/local/mailman/bin:$ENV{'PATH'}"; # FIXME: configurable
    
my $script = "sv_mailman_gray";
my $logfile = "/var/log/sv_database2system.log";

# Import
our $sys_mail_domain;
our $sys_cron_mailman;
our $sys_mailman_dir;

# Preconfigure
my $getopt;
my $help;
my $debug;
my $cron;
my $make_never;
my $make_always; # Not used. For compatibility with sv_post_office

eval {
    $getopt = GetOptions("no-make" => \$make_never,
			 "make-never" => \$make_never,
			 "make-always" => \$make_always,
			 "debug" => \$debug,
			 "cron" => \$cron,
			 "help" => \$help);
};

if($help || !$getopt) {
    print STDERR <<EOF;
Usage: $0 [--help] [--debug] [--cron] [--make-never] [--make-always]

Create with mailman shell command a list for each mailing list found in
the database with status set to 1. 
It will change the status of the created list to 5.

      --help                   Show this help and exit
      --cron                   Option to set when including this script
                               in a crontab
      --no-make, --make-never  Do not run make after processing
      --make-always            Always run make, even if nothing changed

Authors: yeupou\@gnu.org, gray\@gnu.org
EOF
 exit(1);
}

# Test if we should run, according to conffile
exit if ($cron && ! $sys_cron_mailman);


sub Escape {
    my $data = $_[0];
    $data =~ s/\'/\\\'/g;
    return $data;
}


# Log: Starting logging
open (LOG, ">>$logfile");
print LOG strftime "[$script] %c - starting\n", localtime;


# Locks: There are several sv_db2sys scripts but they should not run
#        concurrently.  So we add a lock
AcquireReplicationLock();

my $list_count = 0;
my $initialized = 0;

foreach my $line (GetDB("mail_group_list", 
			"status='1' OR status='0'",
			"group_list_id,list_name,is_public,password,list_admin,description")) {
    chomp($line);
    my ($id, $name, $is_public, $password, $admin, $description) = split(",", $line);
    $admin = GetUserName($admin)."\@".$sys_mail_domain;

    print LOG "DEBUG: $line\n" if $debug;
    
    # Create the list.
    my $listfile = "$sys_mailman_dir/$name";
    my $dirfile = "$sys_mailman_dir/LISTS";

    if (-e $listfile) {
	print LOG strftime "[$script] %c - List file $listfile already exists.\n", localtime;
	next;
    }
    
    open(LIST, "> $listfile") or do {
	print LOG strftime "[$script] %c - Failed to open $listfile for writing.\n", localtime;
	next;
    };

    if (!$initialized) {
        system("cp $dirfile $dirfile.~") if (-e $dirfile);

	open(DIR, ">>$dirfile") or do {
	    print LOG strftime "[$script] %c - Failed to open $dirfile for appending.\n", localtime;
	    close(LIST);
	    last;
	};
	$initialized = 1;
    }

    open(PROG, "newlist -q $name $admin $password |") or do {
	print LOG strftime "[$script] %c - Failed to run newlist.\n", localtime;
	close(LIST);
	unlink($listfile);
	next;
    };
    
    my $state = 0;
    while(<PROG>) {
	/^## $name mailing list/ && ($state = 1); 
	$state == 1 && print LIST $_; 
    }
    close(PROG);
    close(LIST);

    if ($state != 1) {
	print LOG strftime "[$script] %c - Failed to create $listfile.\n", localtime;
	next;
    }
    
    my $dir;
    if ($is_public) {
	$dir = "/var/mailman/archives/html/public";
    } else {
	$dir = "/var/mailman/archives/html/private";
    }
    mkdir($dir."/".$name);
    print LOG strftime "[$script] %c - List $name <$admin> newlist.\n", localtime;

    print DIR "$name\n";
    $list_count++;
    
    # Configure the list.
    
    # Avoid race conditions
    system("/bin/rm", "-rf", "/tmp/mmcfg");
    mkdir("/tmp/mmcfg", 0700)
	or die "Unable to create /tmp/mmcfg safely. Exiting";

    open(TMPCFG, "> /tmp/mmcfg/mmcfg");
    # Always set description
    print TMPCFG "description = '".Escape($description)."'\n";
    # mailman is not useful to fight spam, in fact being forced to use
    # it's interface instead of having a spamassassin doing the job
    # can be seen as a pain.
    print TMPCFG "require_explicit_destination = 0\n";
    # Do not advertise, hide archives, require approval if private list
    print TMPCFG "archive_private = 1\n" unless $is_public;
    print TMPCFG "advertised = 0\n" unless $is_public;
    print TMPCFG "subscribe_policy = 3\n" unless $is_public;
    # Always give access to the member list only to list admins
    print TMPCFG "private_roster = 2\n";
    # Set the message limit size reasonnably big
    ## Set in mm_cfg.py
    ## print TMPCFG "max_message_size = 10240\n";
    close(TMPCFG);
    system("config_list",
	   "--inputfile",
	   "/tmp/mmcfg/mmcfg",
	   $name);
    print LOG strftime "[$script] %c - List $name <$admin> config_list.\n", localtime;

    # Send a mail giving the password 
    my $mail = "Hello,

You requested the creation of the list $name at $sys_mail_domain.

The list administrator password of the mailing list $name is:
                   $password 

You are advised to change the password, and to avoid at any cost using 
a password you use for others important account, as mailman does not
really provide security for these list passwords.

Currently no convenient way to get back your list administrator password
is implemented, so it would be best if you make sure to remember your
list password :-)

Regards,";

    MailSend("", $admin, "Mailman list $name", $mail);
    print LOG strftime "[$script] %c - Mail sent to $admin for list $name.\n", localtime;
    
    # Drawback1: I run here an SQL command to reset the status and to get
    # the admin login for each list. I could be resources consuming, however
    # I think we cannot expect this to be too heavy, not so many list should
    # be created everyday.
    # This scalability issue should be kept in mind, however.
    # Drawback2: currently this script only create list, does not
    # update them. This should be fixed in one way or another.

    SetDBSettings("mail_group_list",
		  "list_name='$name'",
		  "status='5'");
    print LOG strftime "[$script] %c - List $name <$admin> created.\n", localtime;
    
}

close(DIR) if ($initialized);

unless ($make_never) {
    my $output = `make -C /etc/mail aliases.db 2>&1`;
    my $res = $?;
    foreach my $line (split("\n", $output)) {
	print LOG strftime "[$script] $line\n", localtime;
    }
    if ($res != 0) {
	print LOG strftime "[$script] %c - make failed with code $?.\n", localtime;
    }
}

# Final exit
print LOG strftime "[$script] %c - work finished\n", localtime;
print LOG "[$script] ------------------------------------------------------\n";
system("/bin/rm", "-rf", "/tmp/mmcfg");

# END

